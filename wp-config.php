<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */

$host = '';
if (isset($_SERVER['HTTP_HOST']))
    $host = $_SERVER['HTTP_HOST'];

if ($host == 'rocketfactory.artwebit.ru')
{
    define('DB_NAME', 'astepochkin_rf');
    define('DB_USER', 'astepochkin_rf');
    define('DB_PASSWORD', '52HWcZMnlc');
}
else
{
    define('DB_NAME', 'rocketfactory');
    define('DB_USER', 'root');
    define('DB_PASSWORD', 'root');
}





/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8PmoCtBHJJ4c/~>_&I?;<l+9%l2PK#|ymg*Kr&:`3[)}D,sdmX.y2LU[<~uhWI>f');
define('SECURE_AUTH_KEY',  '>TFn$[#,#@Y=x~88_!*7bYEnUXd.hf{k*F8~uU.gTS]x;ZXghp{oVI[wpog;2E+8');
define('LOGGED_IN_KEY',    'eoh$~:<8b|A2*#=jD4#o`*X>D476ht)1x,d$HV^~rx:ck<*0w9x%0e7}*U^7A%hG');
define('NONCE_KEY',        'f@Kq7PBNex(kHKr}3yt.|6g`r<tjE8e3}B2BQM}pT0ba&Ak!I]2`#SyzI;6=V7,r');
define('AUTH_SALT',        '|4YWQD<iYocHm-A&Z_+c?s*&LK/:j-I2B;[3z|*vp5f!7`Rcua(KFZLtQt!1;sbY');
define('SECURE_AUTH_SALT', 'B&7,z/}~>JwB}zu@SKGhWd-Yb%Am8c( /G}zx#M)?H%E9VSg*HG&{hfEqY>-VM@C');
define('LOGGED_IN_SALT',   'dpHi/&I[6Suv,O=`npI@`8 ~m:x!QZ42Ida9>vBjyb)sVxZy;N#b^Dxak!v8U!q(');
define('NONCE_SALT',       'FM_qdBd_1^nOt* ^:G{x<E`jySHBUGq^ D4N8}}z&f#Y@(p4j%v?e5[dWv?1(6F/');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
