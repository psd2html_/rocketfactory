<?php

include( TEMPLATEPATH.'/classes.php' );
include( TEMPLATEPATH.'/widgets.php' );

if (!is_admin())
{
    //wp_deregister_script('jquery');
    //wp_register_script('jquery', ("http://cdn.jquerytools.org/1.1.2/jquery.tools.min.js"), false, '1.3.2');
    //wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"), false, '1.7.2');
    //wp_enqueue_script('jquery');


    //wp_register_script('jquery_min_js',  'https://yastatic.net/jquery/1.11.0/jquery.min.js', false, false, true );
    //wp_register_script('plugins_min_js',  get_template_directory_uri() . '/js/plugins.min.js', false, false, true );
    //wp_register_script('main_js',  get_template_directory_uri() . '/js/main.js', false, false, true );
}

//отключаем jQuery
/*
add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );
function dequeue_jquery_migrate( &$scripts)
{
    if (!(is_admin_bar_showing()))
    {
        $scripts->remove( 'jquery');
        //$scripts-&gt;add( 'jquery', false, array( 'jquery-core' ), '1.11.1? );
    }
}
*/
//отключаем jQuery


if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'id' => 'proccess',
		'name' => 'Этапы реализации проекта',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => ''
	));
    register_sidebar(array(
        'id' => 'order_project',
        'name' => 'Кнопка заказать прокт',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar(array(
        'id' => 'write_message',
        'name' => 'Форма напишите нам',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar(array(
        'id' => 'qtranslate',
        'name' => 'Переключатель языков',
        'before_widget' => '<div class="page-header__lang langbox">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'id' => 'vacancy_form',
        'name' => 'Форма отклик на вакансию',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar(array(
        'id' => 'item_write',
        'name' => 'Пункт меню напишите нам',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'id' => 'title-0',
        'name' => 'Заголовок для проектов на главной',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));


    register_sidebar(array(
        'id' => 'title-1',
        'name' => 'О клиенте / Заказчике',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar(array(
        'id' => 'title-2',
        'name' => 'Задача',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    register_sidebar(array(
        'id' => 'title-3',
        'name' => 'Решение',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'id' => 'socials-icons',
        'name' => 'Социальные иконки',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

}


if ( function_exists( 'add_theme_support' ) ) {

    // Post thumbnail - [post-thumbnail]
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true );

    // [single-post-thumbnail]
	add_image_size( 'single-post-thumbnail', 400, 9999, true );

    // home thumbnail about block
    add_image_size( 'home-thumbnail', 554, 420, true );

    // service thumbnail
    add_image_size( 'service-thumbnail', 590, 682, true );

    // service thumbnail
    add_image_size( 'vacancy-thumbnail', 866, 339, true );
}

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'base' ),
	'secondary' => __( 'Secondary Navigation', 'base' ),
	'footermenu' => __( 'Footer Navigation', 'base' ),
) );


// add [email]...[/email] shortcode
function shortcode_email($atts, $content) {
	$result = '';
	for ($i=0; $i<strlen($content); $i++) {
		$result .= '&#'.ord($content{$i}).';';
	}
	return $result;
}
add_shortcode('email', 'shortcode_email');

// register tag [template-url]
function filter_template_url($text) {
	return str_replace('[template_url]', get_bloginfo('template_url'), $text);
}
add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// register tag [site-url]
function filter_site_url($text) {
	return str_replace('[site-url]', get_bloginfo('url'), $text);
}
add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');



// Register post type - Slider on home
add_action( 'init', 'post_types_slider' );
function post_types_slider() {
    register_post_type( 'type_slider',
        array(
            'labels' => array(
                'name' => __( 'Слайдер' ),
                'singular_name' => __( 'Слайдер' ),
                'add_new_item' => __( 'Добавить новый Слайдер' ),
                'edit_item' => __( 'Редактировать Слайдер' ),
            ),
            'public' => true,
            'taxonomies' => array('post_tag'),
        )
    );
    add_post_type_support( 'type_slider', 'custom-fields' );
    add_post_type_support( 'type_slider', 'title');
    add_post_type_support( 'type_slider', 'editor');
    add_post_type_support( 'type_slider', 'thumbnail');
    add_post_type_support( 'type_slider', 'excerpt');
    add_post_type_support( 'type_slider', 'comments');
    register_taxonomy('provider-state', 'type_slider', array( 'hierarchical' => true, 'label' => 'State', 'query_var' => true, 'rewrite' => true ) );
}
add_theme_support('post-thumbnails', array('type_slider'));


// // Register post type - Projects on home
add_action( 'init', 'post_types_projects' );
function post_types_projects() {
    register_post_type( 'type_projects',
        array(
            'labels' => array(
                'name' => __( 'Проекты' ),
                'singular_name' => __( 'Проекты' ),
                'add_new_item' => __( 'Добавить новый Проект' ),
                'edit_item' => __( 'Редактировать Проект' ),
            ),
            'public' => true,
            'taxonomies' => array('post_tag'),
        )
    );
    add_post_type_support( 'type_projects', 'custom-fields' );
    add_post_type_support( 'type_projects', 'title');
    add_post_type_support( 'type_projects', 'editor');
    add_post_type_support( 'type_projects', 'thumbnail');
    add_post_type_support( 'type_projects', 'excerpt');
    add_post_type_support( 'type_projects', 'comments');
    register_taxonomy('provider-state', 'type_projects', array( 'hierarchical' => true, 'label' => 'State', 'query_var' => true, 'rewrite' => true ) );
}
add_theme_support('post-thumbnails', array('type_projects'));

// Register post type - Services
add_action( 'init', 'post_type_services' );
function post_type_services() {
    register_post_type( 'type_services',
        array(
            'labels' => array(
                'name' => __( 'Услуги' ),
                'singular_name' => __( 'Услуги' ),
                'add_new_item' => __( 'Добавить новый Услуги' ),
                'edit_item' => __( 'Редактировать Услуги' ),
            ),
            'public' => true,
            'taxonomies' => array('post_tag'),
        )
    );
    add_post_type_support( 'type_services', 'custom-fields' );
    add_post_type_support( 'type_services', 'title');
    add_post_type_support( 'type_services', 'editor');
    add_post_type_support( 'type_services', 'thumbnail');
    add_post_type_support( 'type_services', 'excerpt');
    add_post_type_support( 'type_services', 'comments');
    register_taxonomy('provider-state', 'type_services', array( 'hierarchical' => true, 'label' => 'State', 'query_var' => true, 'rewrite' => true ) );
}
add_theme_support('post-thumbnails', array('type_services'));



// Custom Image Widgets Plugin
add_filter('sp_template_image-widget_widget.php', 'my_template_filter');
function my_template_filter($template) {
    return get_template_directory() . '/templates/image-widget.php';
}
