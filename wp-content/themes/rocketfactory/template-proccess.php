<?php
/*
Template Name: Шаблон Реализация
*/
?>
<?php get_header(); ?>
    <!-- page-helper-->
    <div class="page-helper">

        <!-- header-->
        <?php include_once('includes/header.php'); ?>

        <!-- page-content-->
        <div class="page-content">
            <div class="process">

                <?php if ( have_posts() ) : $x=0; ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="page-title">
                            <div class="wrap">
                                <?php the_content(); ?>
                                <a class="process-nav-down scroll-link wow fadeInDown" data-wow-delay="1.2s" href="#step1"><i class="icon icon-arr-down-white"></i></a>
                            </div>
                        </div>

                        <div class="process-steps">
                            <div class="wrap">
                                <?php dynamic_sidebar('proccess'); ?>
                            </div>

                            <!-- Заказать проект -->
                            <div class="wrap">
                                <div class="proccess-btn ta-c">
                                    <?php dynamic_sidebar('order_project'); ?>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
<?php get_footer(); ?>