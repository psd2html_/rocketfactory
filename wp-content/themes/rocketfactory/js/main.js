equalheight = function(container){ 
    var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
         rowDivs.length = 0; // empty the array
         currentRowStart = topPostion;
         currentTallest = $el.height();
         rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

resizeBg = function(imgContainer) {
    $(imgContainer).each(function(indx, el){
        var contWidth = $(el).width();
        var contHeight = $(el).height();
        var aspectRatio = $(el).find('img').width() / $(el).find('img').height();

        if ( (contWidth / contHeight) < aspectRatio ) {
            $(el).find('img').removeClass('bgheight bgwidth').addClass('bgheight');
        } else {
            $(el).find('img').removeClass('bgheight bgwidth').addClass('bgwidth');
        }
    }); 
}

function scrollbarWidth() {
    var block = $('<div>').css({'height':'50px','width':'50px'}),
        indicator = $('<div>').css({'height':'200px'});

    $('body').append(block.append(indicator));
    var w1 = $('div', block).innerWidth();    
    block.css('overflow-y', 'scroll');
    var w2 = $('div', block).innerWidth();
    $(block).remove();
    return (w1 - w2);
}

function popupOpen(e){
    e.preventDefault();
    $('html').addClass('popup-open');
    var id = $(this).attr('href');
    $(id).fadeIn(500);    

    $('.mp-slider').slick('slickPause');
}
function popupClose(e){
    $(this).closest(".popup").fadeOut(500, function(){$('html').removeClass('popup-open');});
    e.preventDefault();

    $('.mp-slider').slick('slickPlay');
}

/*** check window size ***/
function checkWindowSize() {
    width = Math.max($(window).width(), window.innerWidth);
    height = window.innerHeight; 

    //pj-grid
    var pjItemHeight = $(".pj-item-1").width()/2;
    var pjItem2Height = $(".pj-item-1").width();
    $(".pj-item-2, .pj-item-3, .pj-item-4, .pj-item-6, .pj-item-7, .pj-item-8, .pj-item-9, .pj-item-2-mob, .pj-item-4-mob").height(pjItemHeight);
    $(".pj-item-1, .pj-item-5").height(pjItem2Height);    
	if(width < 1000 & width >= 600){
        $(".pj-item-2").clone().addClass("pj-item-2-mob").insertAfter($(".pj-item-6"));
        $(".pj-item-4").clone().addClass("pj-item-4-mob").insertBefore($(".pj-item-7"));
    }else{
        $(".pj-item-2-mob, .pj-item-4-mob").remove();
    } 

    if(width <= 700){
        equalheight(".mp-slide__info");
    }else{
        $(".mp-slide__info").height('auto');
    }

    if(height > 650 && width > 1000){
        $(".mp-slide").outerHeight(height-$(".page-header").outerHeight()-5);    
    }else{
        $(".mp-slide").outerHeight('auto');
    }
    
}
/*** end check window size ***/

$(window).on('load', function () {
    //site pages grid
    $('.site-grid').masonry({
        // options
        columnWidth: $('.site-grid').find('.grid-sizer')[0],
        itemSelector: '.site-item',
    });

    //style for popup
    $('head').append('<style>html.popup-open{margin-right:'+ scrollbarWidth() +'px;}.popup__overlay{right:'+ scrollbarWidth() +'px;}</style>');
});

$(document).ready(function() {
	width = Math.max($(window).width(), window.innerWidth);
    height = $(window).height();    

    if(window.location.hash.length > 0) {
        setTimeout(function(){
            $('html, body').stop().animate({
                scrollTop: ($(window.location.hash).offset().top)
            }, 700, 'easeOutQuad');           
        }, 150);        
    }

    checkWindowSize();

    //mp-slider
    $('.mp-slider').on('init reinit', function(event, slick){               
    	var dotsLeftPos = (width - $(".wrap").width())/2;
    	$(".slick-dots").css('left', dotsLeftPos);
    	$(".mp-slider").addClass('slide' + (slick.currentSlide + 1) + '-active');

        $(".mp-slider .slick-current").find('[data-animation-in]').each(function(indx, el){
            var animationIn = $(el).data('animation-in');
            var animationDelay = $(el).data('delay-in');
            $(el).css('animation-delay', animationDelay).addClass('animated ' + animationIn);
        }); 
    });
    
    if($('body').find('.mp-slider').length > 0){
        $('.mp-slider').slick({
            dots: true, 
            arrows: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000, 
            pauseOnHover: false,
            fade: true,
            speed: 1000,
            lazyLoad: 'progressive',
            touchThreshold: 2
        });
    }   

    $('.mp-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        setTimeout(function(){
            $(".mp-slider").removeClass('slide' + (currentSlide + 1) + '-active').addClass('slide' + (nextSlide + 1) + '-active');
        }, 1500);

        $(".mp-slider .slick-slide[data-slick-index="+currentSlide+"]").find(".animated").each(function(indx, el){
            var animationIn = $(el).data('animation-in');
            var animationOut = $(el).data('animation-out');
            var animationDelay = $(el).data('delay-out');
            $(el).css('animation-delay', animationDelay).removeClass('animated ' + animationIn).addClass('animated ' + animationOut);
        }); 
    });

    $('.mp-slider').on('afterChange', function(event, slick, currentSlide){
        $(".mp-slider .slick-current").find('[data-animation-in]').each(function(indx, el){
            var animationIn = $(el).data('animation-in');
            var animationDelay = $(el).data('delay-in');
            $(el).css('animation-delay', animationDelay).addClass('animated ' + animationIn);
        }); 
        setTimeout(function(){
            $(".mp-slider .slick-slide:not(.slick-current)").find(".animated").each(function(indx, el){
                var animationOut = $(el).data('animation-out');
                $(el).removeClass('animated ' + animationOut);
            }); 
        }, 1000);
        
    });

    //scroll-link
    $(".scroll-link").on('click', function(e){
    	//if($('.page').hasClass('mainpage')){
    		e.preventDefault();
    	//}
        var anchor = jQuery(this);
        $('html, body').stop().animate({
            scrollTop: ($(anchor.attr('href')).offset().top)
        }, 700, 'easeOutQuad');

        if($('body').find(".pj-page").length > 0) {
        	setTimeout(function(){$('html, body').stop().animate({
	            scrollTop: ($(anchor.attr('href')).offset().top)
	        }, 500, 'easeOutQuad');}, 500);
        }       
                
        if($("body").hasClass("open-m-m")){
            $("body").removeClass("open-m-m");
            $(".page-header__menu").fadeOut();  
        }
    });

    //mobmenu
    $(".mobmenu-toggle").on('click', function(e){
        $("body").addClass("open-m-m");
        $(".page-header__menu").fadeIn();             
    });
    $(".mobmenu-close").on('click', function(e){
        $("body").removeClass("open-m-m");
        $(".page-header__menu").fadeOut();             
    });    

    //wow
    new WOW().init();

    // pj-nav
    if($('body').find(".pj-page").length > 0) {
        var laptopMargin = parseInt($(".pj-page__sec--laptop").css('margin-top'));
        var pos = $(".pj-page__sec--task").offset().top - height;

        $(window).scroll(function () {
            var bottomPos = $("#pj-plan").offset().top + $("#pj-plan").outerHeight() - $(".pj-page__nav").height() - 110;
            if ( $(this).scrollTop() >= $("#pj-info").offset().top && $(this).scrollTop() < bottomPos) {
                $('.pj-page__nav').removeClass('bottom').addClass('fixed');
            } else if( $(this).scrollTop() >= bottomPos ) {
                $('.pj-page__nav').removeClass('fixed').addClass('bottom');
            } else {
                $('.pj-page__nav').removeClass('fixed');
            }
        });

        // waypoints
        var waypoints = $('.pj-page__content-nav .pj-page__sec').waypoint({
            handler: function() {
                var secId = this.element.id;
                $(".pj-page__nav-link").removeClass('active');
                setTimeout(function(){
                    $(".pj-page__nav-link[href='#"+secId+"']").addClass('active');
                }, 50);
            },
            offset: 100
        });

        if($('body').find(".pj-page").length > 0) {
            // laptop parallax
            var scrollorama = $.scrollorama({enablePin:false, blocks:'.pj-page__content-nav' });
            scrollorama.animate('.pj-page__sec--laptop',{ delay: 0, duration: 500, property: 'margin-top', start: laptopMargin, end: 0 });
        }
    }

    //form
    $(".phone-mask").mask("+7 (999) 999-99-99");

    //validate contactform
    $.extend(jQuery.validator.messages, {
        required: "Заполните поле",
        remote: "Please fix this field.",
        email: "Проверьте почту",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Введите не менее, чем {0} символа"),
    });

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zа-я\s]+$/i.test(value);
    }, "Поле содержит недопустимые символы"); 
    $.validator.addMethod("phoneRUS", function(phone_number, element) {
        return this.optional(element) || phone_number.length > 9 && 
        phone_number.match(/^\+?7\s?\(\d{3}\)\s?\d{1,3}-\d{2}-\d{2}$/);
    }, "Введите телефон в формате +7 (___) ___-__-__");

    $.validator.addClassRules({
        name : {
            required : true, 
            minlength: 2,
            lettersonly: true
        },
        email : {
            required: true,
            email: true
        }
    });
    $('form').each(function () {
        $(this).validate({
            errorClass : "input-error",
            //focusCleanup: true,
            errorPlacement: function(error, element) {
                error.insertBefore( element.parent(".inputbox-wrap") );
            },

			
            /*
            submitHandler: function(form) { 
                $.ajax({
                    type: "GET",
                    url: "./",
                    data: $(form).serialize(),
                    success: function () {
                        $(form)[0].reset();
                        if($(form).parents(".popup-form").length > 0){
                            $(".popup-form").append('<div class="form-alert success">Ваше сообщение успешно отправлено!</div>'); // if success
                            //$(".popup-form").append('<div class="form-alert error">Что-то пошло не так. Попробуйте еще раз.</div>'); // if error

                            $(".popup").delay(2500).fadeOut(500, function(){$('html').removeClass('popup-open');});
                            $(".popup").find(".form-alert").delay(4000).fadeOut();
                        } else{
                            $(form).append('<div class="form-alert success">Ваше сообщение успешно отправлено!</div>'); // if success
                            //$(form).append('<div class="form-alert error">Что-то пошло не так. Попробуйте еще раз.</div>'); // if error
                            $(form).find(".form-alert").delay(3000).fadeOut();
                        }
                    }
                 });
                return false; // required to block normal submit since you used ajax           
            }
            */
			
        });
    });

    //input file
    $('.inputbox-file').on('change', function(){
        var value = $(this).val().split('\\').pop();
        $(this).prev().hide();
        $(this).next().text(value).show();
    });

    //popup
    $('li.popup-link a').on('click', popupOpen);
    $('a.popup-link').on('click', popupOpen);
    $(".popup__close").on('click', popupClose);
    $(".popup__overlay").on('click', popupClose);    
    
});

$(window).on("resize orientationchange", function(e) { 
    checkWindowSize();

    if($('body').find('.mp-slider').length > 0){

        if(width < 750){
             $('.mp-slider')[0].slick.refresh();
         }       
    
        var dotsLeftPos = (width - $(".wrap").width())/2;
        $(".slick-dots").css('left', dotsLeftPos);
    }   

});