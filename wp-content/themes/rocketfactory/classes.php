<?php

class ControlPanel
{
    // Устанавливаем значения по умолчанию
    var $default_settings = array(
        'phone' => '495 122-12-12',
        'email' => 'info@site.ru'
    );
    var $options;

    function ControlPanel() {
        add_action('admin_menu', array(&$this, 'add_menu'));
        if (!is_array(get_option('themadmin')))
            add_option('themadmin', $this->default_settings);
        $this->options = get_option('themadmin');
    }

    function add_menu() {
        add_theme_page('WP Theme Options', 'Опции темы', 8, "themadmin", array(&$this, 'optionsmenu'));
    }

    // Сохраняем значения формы с настройками
    function optionsmenu() {
        if ($_POST['ss_action'] == 'save') {
            $this->options["phone"] = $_POST['cp_phone'];
            $this->options["phone2"] = $_POST['cp_phone2'];
            $this->options["address"] = $_POST['cp_address'];
            $this->options["copyright"] = $_POST['cp_copyright'];
            $this->options["email"] = $_POST['cp_email'];
            $this->options["facebook"] = $_POST['cp_facebook'];
            $this->options["vkontakte"] = $_POST['cp_vkontakte'];
            $this->options["twitter"] = $_POST['cp_twitter'];
            $this->options["google"] = $_POST['cp_google'];
            update_option('themadmin', $this->options);
            echo '<div class="updated fade" id="message" style="background-color: rgb(255, 251, 204); width: 400px; margin-left: 17px; margin-top: 17px;"><p>Ваши изменения <strong>сохранены</strong>.</p></div>';
        }
        // Создаем форму для настроек
        echo '<form action="" method="post" class="themeform">';
        echo '<input type="hidden" id="ss_action" name="ss_action" value="save">';

        print '<div class="cptab"><br />
 <b>Настройки темы</b>
 <br />

 <h3>Контакты</h3>
 <p><input placeholder="Телефон-1" style="width:300px;" name="cp_phone" id="cp_phone" value="'.$this->options["phone"].'"><label> - телефон-1</label></p>
 <p><input placeholder="Телефон-2" style="width:300px;" name="cp_phone2" id="cp_phone2" value="'.$this->options["phone2"].'"><label> - телефон-2</label></p>
 <p><input placeholder="Email" style="width:300px;" name="cp_email" id="cp_email" value="'.$this->options["email"].'"><label> - email</label></p>
 <p><input placeholder="Адрес" style="width:300px;" name="cp_address" id="cp_address" value="'.$this->options["address"].'"><label> - Адрес</label></p>
 <p><input placeholder="Копирайт" style="width:300px;" name="cp_copyright" id="cp_copyright" value="'.$this->options["copyright"].'"><label> - Копирайт</label></p>
 </div><br />';
        echo '<input type="submit" value="Сохранить" name="cp_save" class="dochanges" />';
        echo '</form>';
    }
}
$cpanel = new ControlPanel();
$mytheme = get_option('themadmin');


//Custom Menu Walker
class Custom_Walker_Nav_Menu extends Walker_Nav_Menu
{
    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-menu\">\n";
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    function start_el(&$output, $item, $depth, $args) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';




        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes . ' class="'. $item->xfn  .'">';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function end_el(&$output, $item, $depth) {
        $output .= "</li>\n";
    }
}
