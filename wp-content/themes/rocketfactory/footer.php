        <!-- page-footer-->
        <footer class="page-footer">
            <div class="wrap clearfix">
                <div class="page-footer__l clearfix">


                    <div class="page-footer__contact">
                        <?php global $mytheme; ?>
                        <?php echo $mytheme['address']; ?>
                        <br>
                        <a class='phone-link' href='tel:+7<?php echo substr(str_replace("-", "", str_replace(" ", "", str_replace(")", "" , str_replace("(", "", $mytheme['phone'])))), 1); ?>'><?php echo $mytheme['phone']; ?></a>

                        <br>
                        <a class='phone-link' href='tel:+7<?php echo substr(str_replace("-", "", str_replace(" ", "", str_replace(")", "" , str_replace("(", "", $mytheme['phone2'])))), 1); ?>'><?php echo $mytheme['phone2']; ?></a>
                        <br>
                        <a href="mailto:<?php echo stripslashes($mytheme['email']); ?> "><?php echo stripslashes($mytheme['email']); ?> </a>
                        <br>
                        <span class="copyright"><?php echo stripslashes($mytheme['copyright']); ?></span>
                    </div>

                    <div class="page-footer__contact"></div>

                    <!--menu-->
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footermenu',
                            'menu_class'     => 'footermenu',
                            'link_before'    => '',
                            'link_after'     => '',
                            'container'      => ''
                        ));
                    ?>
                </div>

                <div class="page-footer__r">
                    <?php dynamic_sidebar('Социальные иконки'); ?>
                </div>

            </div>
        </footer>
    </div>

    <!-- POPUP -->
    <?php include_once('includes/popup-feedback-form.php'); ?>
    <?php include_once('includes/popup-vacancy-form.php'); ?>


    <!-- scripts--><!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->

    <script src="https://yastatic.net/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/plugins.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
    <?php wp_footer(); ?>
</body>
</html>