<?php
/*
Template Name: Шаблон Контакты
*/
?>
<?php get_header(); ?>

    <!-- page-helper-->
    <div class="page-helper">

        <!-- header-->
        <?php include_once('includes/header.php'); ?>

        <!-- page-content-->
        <div class="page-content">

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="contact">
                        <div class="wrap">
                        <h1 class="page-title wow fadeInUp"><?php the_title(); ?></h1>

                            <?php global $mytheme; ?>

                            <div class="contact-data">
                                <ul class="contact-data__list">
                                    <li class="wow fadeInLeft" data-wow-delay="0.3s">
                                        <div class="contact-data__icon"><i class="icon icon-phone"></i></div>
                                        <a class="phone-link" href="tel:+7<?php echo substr(str_replace("-", "", str_replace(" ", "", str_replace(")", "" , str_replace("(", "", $mytheme['phone'])))), 1); ?>"><?php echo $mytheme['phone']; ?></a>
                                        <br>
                                        <a class="phone-link" href="tel:+7<?php echo substr(str_replace("-", "", str_replace(" ", "", str_replace(")", "" , str_replace("(", "", $mytheme['phone2'])))), 1); ?>"><?php echo $mytheme['phone2']; ?></a>
                                    </li>
                                    <li class="wow fadeInLeft" data-wow-delay="0.5s">
                                        <div class="contact-data__icon"><i class="icon icon-envelope"></i></div>
                                        <a href="mailto:<?php echo $mytheme['email']; ?>"><?php echo $mytheme['email']; ?></a>
                                    </li>
                                    <li class="wow fadeInLeft" data-wow-delay="0.7s">
                                        <div class="contact-data__icon"><i class="icon icon-marker"></i></div>
                                        <?php echo $mytheme['address']; ?>
                                    </li>
                                </ul>
                            </div>


                            <div class="grid-row clearfix">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </div>

<?php get_footer(); ?>
