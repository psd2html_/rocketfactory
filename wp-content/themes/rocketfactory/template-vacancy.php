<?php
/*
Template Name: Шаблон Вакансии
*/
?>
<?php get_header(); ?>
    <!-- page-helper-->
    <div class="page-helper">

        <!-- header-->
        <?php include_once('includes/header.php'); ?>

        <!-- page-content-->
        <div class="page-content">

        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <div class="vacancy">
                    <div class="page-title">
                        <div class="wrap">
                            <h1 class="wow fadeInUp"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <div class="vacancy-about">
                        <div class="wrap clearfix">

                            <?php
                                $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
                                $image = wp_get_attachment_image_src($post_thumbnail_id, 'vacancy-thumbnail');
                            ?>

                            <div class="vacancy-about__img wow fadeInRight" data-wow-delay="0.6s">
                                <img src="<?php echo $image[0]; ?>" alt="">
                            </div>

                            <div class="vacancy-about__text wow fadeIn" data-wow-delay="0.3s">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="vacancy-list">
                        <div class="wrap">
                            <div class="vacancy-list__row clearfix">
                                <div class="vacancy-list__col wow fadeInUp" data-wow-delay="0.3s">
                                    <?php the_field('блок_1'); ?>
                                </div>
                                <div class="vacancy-list__col wow fadeInUp" data-wow-delay="0.4s">
                                    <?php the_field('блок_2'); ?>
                                </div>
                            </div>
                            <div class="vacancy-list__row clearfix">
                                <div class="vacancy-list__col wow fadeInUp" data-wow-delay="0.5s">
                                    <?php the_field('блок_3'); ?>
                                </div>
                                <div class="vacancy-list__col wow fadeInUp" data-wow-delay="0.6s">
                                    <?php the_field('блок_4'); ?>
                                </div>
                            </div>
                        </div>

                        <!-- Кнопка Откликнуться -->
                        <?php the_field('кнопка_откликнуться'); ?>

                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>