<div class="popup" id="feedback-form">
    <div class="popup__container">
        <div class="popup__content">
            <?php dynamic_sidebar('Форма напишите нам'); ?>
        </div>
    </div>
    <button class="popup__close"></button>
    <div class="popup__overlay"></div>
</div>