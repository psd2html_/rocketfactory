<div class="popup" id="feedback-vacancy">
    <div class="popup__container">
        <div class="popup__content">
            <?php dynamic_sidebar('Форма отклик на вакансию'); ?>
        </div>
    </div>
    <button class="popup__close"></button>
    <div class="popup__overlay"></div>
</div>