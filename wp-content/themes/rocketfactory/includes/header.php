<header class="page-header">
    <div class="wrap clearfix"><a class="page-header__logo-link" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?>"><i class="logo-star logo-star-1"></i><i class="logo-star logo-star-2"></i><i class="logo-star logo-star-3"></i><i class="logo-star logo-star-4"></i></a>

        <!-- menu-->
        <button class="mobmenu-toggle hamburger hamburger--squeeze"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
        <div class="page-header__menu">


            <?php if (is_front_page()) : ?>

                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'menu_class'     => 'mainmenu',
                        'link_before'    => ' <span>',
                        'link_after'     => '</span>',
                        'container'      => '',
                        'walker'         => new Custom_Walker_Nav_Menu()
                    ));
                ?>

            <?php else : ?>

                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'secondary',
                        'menu_class'     => 'mainmenu',
                        'link_before'    => ' <span>',
                        'link_after'     => '</span>',
                        'container'      => '',
                        'walker'         => new Custom_Walker_Nav_Menu()
                    ));
                ?>

            <?php endif; ?>


            <button class="mobmenu-close"><i class="icon icon-close-yellow"></i></button>
        </div>

        <!-- header rightblock-->
        <div class="page-header__r clearfix">

            <!--lang-->
            <?php dynamic_sidebar('Переключатель языков'); ?>
            <?php dynamic_sidebar('Пункт меню напишите нам'); ?>

        </div>
    </div>
</header>