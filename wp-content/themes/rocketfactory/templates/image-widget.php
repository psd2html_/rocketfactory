<?php
// Block direct requests
if ( !defined('ABSPATH') )
    die('-1');

// $this->widget_options['classname']
?>

<div class="process-step process-step-<?php echo $linkid; ?>" id="step<?php echo $linkid; ?>">
    <div class="process-step__img<?php if ($align == 'left') echo '-1'; ?>">
        <?php echo $this->get_image_html( $instance, true ); ?>
    </div>
    <h2 class="process-step__title wow fadeInLeft" data-wow-delay="0.3s">
        <span class="process-step__title-num"><?php echo $linkid; ?></span>
        <?php echo $title; ?>
    </h2>
    <ul class="wow fadeIn" data-wow-delay="0.6s">
        <?php echo wpautop( $description ); ?>
    </ul>
</div>