<?php
/*
Template Name: Home Template
*/
?>
<?php get_header(); ?>
    <!-- page-helper-->
    <div class="page-helper">

        <!-- header-->
        <?php include_once('includes/header.php'); ?>

        <!-- page-content-->
        <div class="page-content mp">
            <!-- mp-slider-->
            <div class="mp-slider">

                <?php
                    query_posts(array(
                            'post_type' => 'type_slider',
                            'post_status' => 'publish',
                            'orderby' => 'date',
                            'order' => 'DESC',
                        )
                    );
                    $x=0;
                ?>
                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php
                            $x++;

                            // Custom Fields
                            $image = get_field('большая_картинка_слайдера');
                            $image = $image['url'];

                            $image_pc = get_field('картинка_pc');
                            $image_pc = $image_pc['url'];

                            $image_ipad = get_field('картинка_ipad');
                            $image_ipad = $image_ipad['url'];

                            $image_iphone = get_field('картинка_iphone');
                            $image_iphone = $image_iphone['url'];

                        ?>
                        <div class="mp-slide mp-slide<?php echo $x; ?>">
                            <div class="mp-slide__bg" style="background-image:url(<?php echo $image; ?>)"></div>
                            <div class="wrap clearfix">
                                <div class="mp-slide__info">
                                    <h3 data-animation-in="fadeInUp" data-animation-out="fadeOut" data-delay-in="0.1s" data-delay-out="0.7s"><?php the_title(); ?></h3>
                                    <div class="mp-slide__desc" data-animation-in="fadeInUp" data-animation-out="fadeOut" data-delay-in="0.25s" data-delay-out="0.8s">
                                        <?php the_content(); ?>
                                    </div>
                                    <a class="btn mp-slide__btn" href="<?php the_field('ссылка_на_проект'); ?>" data-animation-in="zoomIn" data-animation-out="zoomOut" data-delay-in="0.3s" data-delay-out="<?php if ($x==1) echo '0.9s'; else echo '0.5s'; ?>">
									<?php remove_filter( 'the_content', 'wpautop' ); ?>
									<?php echo apply_filters('the_content', '[:en]See the project[:ru]Смотреть проект[:]'); ?>
									<?php add_filter( 'the_content', 'wpautop' ); ?>
									</a>
                                </div>
                                <div class="mp-slide__gadgets clearfix">
                                <div class="mp-slide__pc" data-animation-in="fadeInRightBig" data-animation-out="fadeOutRightBig" data-delay-in="0.7s" data-delay-out="0.1s"><img src="<?php echo $image_pc; ?>" alt="<?php the_title(); ?>"></div>
                                <div class="mp-slide__ipad" data-animation-in="fadeInRightBig" data-animation-out="fadeOutRightBig" data-delay-in="1s" data-delay-out="0.4s"><img src="<?php echo $image_ipad; ?>" alt="<?php the_title(); ?>"></div>
                                <div class="mp-slide__iphone" data-animation-in="fadeInRightBig" data-animation-out="fadeOutRightBig" data-delay-in="1.3s" data-delay-out="0.3s"><img src="<?php echo $image_iphone; ?>" alt="<?php the_title(); ?>"></div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>

            </div>

            <!-- mp-pj-->
            <div class="mp-pj" id="pj">
                <h2 class="wow fadeInUp"><?php dynamic_sidebar('Заголовок для проектов на главной'); ?></h2>
                <div class="pj-grig">
                    <div class="wrap">
                        <div class="pj-box clearfix">
                            <?php
                                query_posts(array(
                                        'post_type'     => 'type_projects',
                                        'post_status'   => 'publish',
                                        'orderby'       => 'date',
                                        'order'         => 'DESC',
                                    )
                                );
                                $x=0;
                            ?>
                            <?php if ( have_posts() ) : ?>
                                <?php while ( have_posts() ) : the_post(); ?>

                                    <?php
                                        // Custom Fields
                                        $image = get_field('картинка_на_главной');
                                        $image = $image['url'];
                                    ?>

                                    <a class="pj-item <?php the_field('класс'); ?> wow fadeIn" href="<?php the_permalink(); ?>" style="background-image:url(<?php echo $image; ?>)" data-wow-delay="<?php the_field('задержка_анимации'); ?>">
                                        <div class="pj-item__title">
                                        <div class="pj-item__name"><?php the_title(); ?></div>
                                            <div class="pj-item__desc"><?php the_field('слоган'); ?></div>
                                        </div>
                                        <div class="pj-item__hover">
                                            <div class="pj-item__hover-text">
                                                <div class="pj-item__name"><?php the_title(); ?></div><?php echo apply_filters('the_content', '[:en]learn more[:ru]подробнее[:]'); ?>
                                            </div>
                                        </div>
                                    </a>

                                <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>

                        </div>
                    </div>
                </div>
            </div>

            <!-- about-->
            <div class="wrap">
                <div class="mp-about clearfix" id="about">

                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <h2 class="wow fadeInUp"><?php the_title(); ?></h2>
                            <div class="mp-about__img wow fadeInRight" data-wow-delay="0.6s"><?php the_post_thumbnail('home-thumbnail'); ?></div>
                            <div class="mp-about__text wow fadeIn" data-wow-delay="0.3s">
                                <?php the_content(); ?>
                            </div>

                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
<?php get_footer(); ?>