<?php get_header(); ?>
<!-- page-helper-->
<div class="page-helper">

    <!-- header-->
    <?php include_once('includes/header.php'); ?>

    <!-- page-content-->
    <div class="page-content">

        <?php if ( have_posts() ) : ?>

            <div class="vacancy">
                <div class="page-title">
                    <div class="wrap">
                        <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
                        <?php /* If this is a category archive */ if (is_category()) { ?>
                            <h1 class="wow fadeInUp">Записи категории &#8216;<?php single_cat_title(); ?>&#8217;</h1>
                            <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
                            <h1 class="wow fadeInUp">Записи тега &#8216;<?php single_tag_title(); ?>&#8217;</h1>
                            <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
                            <h1 class="wow fadeInUp">Архив за <?php the_time('F jS, Y'); ?></h1>
                            <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
                            <h1 class="wow fadeInUp">Архив за <?php the_time('F, Y'); ?></h1>
                            <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
                            <h1 class="wow fadeInUp">Архив за <?php the_time('Y'); ?></h1>
                            <?php /* If this is an author archive */ } elseif (is_author()) { ?>
                            <h1 class="wow fadeInUp">Архив автора</h1>
                            <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
                            <h1 class="wow fadeInUp">Архив блога</h1>
                        <?php } ?>
                    </div>
                </div>
                <div class="vacancy-about">
                    <div class="wrap clearfix">
                        <div class="vacancy-about__text wow fadeIn" data-wow-delay="0.3s">
                            <!-- Контент  -->
                            <?php while ( have_posts() ) : the_post(); ?>
                                <div class="post" id="post-<?php the_ID(); ?>">
                                    <div class="title">
                                        <h2><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
                                        <p class="info"><strong class="date"><?php the_time('F jS, Y') ?></strong> от <?php the_author(); ?></p>
                                    </div>
                                    <div class="content">
                                        <?php the_content('Далее &raquo;'); ?>
                                    </div>
                                    <div class="meta">
                                        <ul>
                                            <li>Категория <?php the_category(', ') ?></li>
                                            <li><?php comments_popup_link('Нет коментариев', '1 коментарий', '% коментариев'); ?></li>
                                            <?php the_tags('<li>Теги: ', ', ', '</li>'); ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endwhile; ?>

                            <div class="navigation">
                                <div class="next"><?php next_posts_link('Предыдущий &raquo;') ?></div>
                                <div class="prev"><?php previous_posts_link('&laquo; Следующий') ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <?php else : ?>

            <div class="post">
                <div class="title">
                    <h1>Не найдено</h1>
                </div>
                <div class="content">
                    <p>Извините, ничего не найдено</p>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>