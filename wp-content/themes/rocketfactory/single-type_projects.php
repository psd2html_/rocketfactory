<?php get_header(); ?>
<!-- page-helper-->
<div class="page-helper">

    <!-- header-->
    <?php include_once('includes/header.php'); ?>

    <!-- page-content-->
    <div class="page-content">
        <?php // класс страницы ?>
        <?php $ind_class_page = get_field('класс_страницы'); ?>
        <div class="pj-page <?php echo $ind_class_page; ?>">
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <!-- pj header-->
                    <?php $image_top_project = get_field('картинка_в_шапке_проекта'); ?>
                    <?php if (isset($image_top_project) and !empty($image_top_project)) : ?>
                        <div class="pj-page__header" style="background-image:url('<?php echo $image_top_project['url']; ?>')">
                            <div class="pj-page__header-img wow slideInUpCenter" data-wow-delay="0.1s">
                                <div class="wrap"><img src="<?php echo $image_top_project['url']; ?>" alt="senso-header"></div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <!-- pj content-->
                    <div class="pj-page__content">
                        <div class="pj-page__content-nav">
                            <ul class="pj-page__nav">
                                <li><a class="scroll-link pj-page__nav-link active" href="#pj-info"><i class="icon icon-i"></i></a></li>
                                <li><a class="scroll-link pj-page__nav-link" href="#pj-task"><i class="icon icon-pen"></i></a></li>
                                <li><a class="scroll-link pj-page__nav-link" href="#pj-plan"><i class="icon icon-key"></i></a></li>
                            </ul>

                            <?php // "О клиенте / Заказчике" ?>
                            <?php $about_client = get_field('о_клиенте_и_заказчике'); ?>
                            <?php if (isset($about_client) and !empty($about_client)) : ?>
                                <div class="pj-page__sec pj-page__sec--about" id="pj-info">
                                    <div class="wrap">
                                        <div class="pj-page__sec-in">
                                            <h2 class="wow fadeInLeft">
                                                <span class="pj-page__title-icon">
                                                    <i class="icon icon-i"></i>
                                                </span><?php dynamic_sidebar('О клиенте / Заказчике'); ?>
                                            </h2>

                                            <div class="pj-page__sec-text wow fadeIn" data-wow-delay="0.4s">
                                                <?php echo html_entity_decode($about_client) ; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php // Картинка под блоком "О клиенте / Заказчике" ?>
                            <?php $image_about_client = get_field('картинка_о_клиенте_и_заказчике'); ?>
                            <?php if (isset($image_about_client) and !empty($image_about_client)) : ?>
                                <div class="pj-page__fixed-b">
                                    <div class="pj-page__sec pj-page__sec--laptop">
                                        <div class="wrap"><img src="<?php echo $image_about_client['url']; ?>" alt="<?php the_title(); ?>"></div>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php // Задача ?>
                            <?php $task = get_field('задача'); ?>
                            <?php if (isset($task) and !empty($task)) : ?>
                                <div class="pj-page__sec pj-page__sec--task" id="pj-task">
                                    <div class="wrap">
                                        <div class="pj-page__sec-in">
                                            <h2 class="wow fadeInLeft">
                                                <span class="pj-page__title-icon">
                                                    <i class="icon icon-pen"></i>
                                                </span><?php dynamic_sidebar('Задача'); ?>
                                            </h2>
                                            <div class="pj-page__sec-text wow fadeIn" data-wow-delay="0.4s">
                                                <?php echo html_entity_decode($task) ; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php // Картинка под блоком "Задача" ?>
                            <?php $image_task = get_field('картинка_задача'); ?>
                            <?php if (isset($image_task) and !empty($image_task)) : ?>
                                <div class="pj-page__sec-img">
                                    <div class="wrap"><img src="<?php echo $image_task['url']; ?>" alt="<?php the_title(); ?>"></div>
                                </div>
                            <?php endif; ?>

                            <?php // Решение ?>
                            <?php $solution  = get_field('решение'); ?>
                            <?php if (isset($solution) and !empty($solution)) : ?>
                                <div class="pj-page__sec pj-page__sec--plan" id="pj-plan">
                                    <div class="wrap">
                                        <div class="pj-page__sec-in">
                                            <h2 class="wow fadeInLeft">
                                                <span class="pj-page__title-icon">
                                                    <i class="icon icon-key"></i>
                                                </span><?php dynamic_sidebar('Решение'); ?>
                                            </h2>
                                            <div class="pj-page__sec-text wow fadeIn" data-wow-delay="0.4s">
                                                <?php echo html_entity_decode($solution) ; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>

                        <?php // картинка_решение  ?>
                        <?php $image_solution = get_field('картинка_решение'); ?>
                        <?php if (isset($image_task) and !empty($image_solution)) : ?>
                            <div class="pj-page__sec-img">
                                <div class="wrap"><img src="<?php echo $image_solution['url']; ?>" alt="<?php the_title(); ?>"></div>
                            </div>
                        <?php endif; ?>



                        <?php the_content(); ?>


                    </div>


                    <?php // не работает перевод с плагином "произвольные поля" ?>
                    <?php /* $link_order = get_field('ссылка_на_проект_и_кнопка_заказа'); ?>
                    <?php if (isset($link_order) and !empty($link_order)) : ?>
                        <?php echo $link_order; ?>
                    <?php endif; */ ?>

                    <?php // поэтому делаем через excerpt ?>
                    <?php the_excerpt(); ?>


                <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>
