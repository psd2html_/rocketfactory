<?php get_header(); ?>
<!-- page-helper-->
<div class="page-helper">

    <!-- header-->
    <?php include_once('includes/header.php'); ?>

    <!-- page-content-->
    <div class="page-content">
        <?php if ( have_posts() ) : ?>
            <div class="vacancy">
                <div class="page-title">
                    <div class="wrap">
                        <h1 class="wow fadeInUp"><?php the_title(); ?></h1>
                    </div>
                </div>
                <div class="vacancy-about">
                    <div class="wrap clearfix">
                        <div class="vacancy-about__text wow fadeIn" data-wow-delay="0.3s">
                            <!-- �������  -->
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="post" id="post-<?php the_ID(); ?>">
                                    <div class="title">
                                        <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                                        <p class="info"><strong class="date"><?php the_time('F jS, Y') ?></strong> by <?php the_author(); ?></p>
                                    </div>
                                    <div class="content">
                                        <?php the_content('����� &raquo;'); ?>
                                    </div>
                                    <div class="meta">
                                        <ul>
                                            <li>��������� <?php the_category(', ') ?></li>
                                            <li><?php comments_popup_link('��� �����������', '1 ����������', '% �����������'); ?></li>
                                            <?php the_tags('<li>����: ', ', ', '</li>'); ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endwhile; ?>

                            <div class="navigation">
                                <div class="next"><?php next_posts_link('���������� &raquo;') ?></div>
                                <div class="prev"><?php previous_posts_link('&laquo; ���������') ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <?php else : ?>

            <div class="post">
                <div class="title">
                    <h1>�� �������</h1>
                </div>
                <div class="content">
                    <p>��������, ������ �� �������</p>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>