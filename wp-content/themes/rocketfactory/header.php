<!DOCTYPE html>
<html lang="ru">
<head><!-- frontend by Alena Tretiakova, lemweb.ru -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <!-- favicon set -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/images/favicon/manifest.json">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon.ico">
    <meta name="msapplication-config" content="<?php bloginfo('template_url'); ?>/images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- end favicon set -->
    <!-- fonts -->
    <!-- font-family: 'Open Sans', sans-serif;-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700&amp;amp;subset=cyrillic" rel="stylesheet">
    <!-- css-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/custom.css">
    <?php wp_head(); ?>
</head>
<body>
<div class="page">