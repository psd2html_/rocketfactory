<?php get_header(); ?>
    <!-- page-helper-->
    <div class="page-helper">

        <!-- header-->
        <?php include_once('includes/header.php'); ?>

        <!-- page-content-->
        <div class="page-content">

            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="vacancy">
                        <div class="page-title">
                            <div class="wrap">
                                <h1 class="wow fadeInUp"><?php the_title(); ?></h1>
                            </div>
                        </div>
                        <div class="vacancy-about">
                            <div class="wrap clearfix">
                                <div class="vacancy-about__text wow fadeIn" data-wow-delay="0.3s">
                                    <!-- Контент  -->
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
<?php get_footer(); ?>