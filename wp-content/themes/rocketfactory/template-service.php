<?php
/*
Template Name: Шаблон Услуги
*/
?>
<?php get_header(); ?>

    <!-- page-helper-->
    <div class="page-helper">

        <!-- header-->
        <?php include_once('includes/header.php'); ?>

        <!-- page-content-->
        <div class="page-content">
            <div class="service">

                <?php if ( have_posts() ) : ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <div class="page-title">
                            <div class="wrap">
                                <h1 class="wow fadeInUp"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>


                <div class="service-list">

                    <?php
                        query_posts(array(
                                'post_type' => 'type_services',
                                'post_status' => 'publish',
                                'orderby' => 'date',
                                'order' => 'DESC',
                            )
                        );
                        $x=1;
                    ?>
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post(); $x++; ?>

                            <?php
                                $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
                                $image = wp_get_attachment_image_src($post_thumbnail_id, 'service-thumbnail');
                            ?>

                            <div class="service-item">
                                <div class="wrap clearfix">
                                    <div class="service-item__img wow <?php if ($x%2 == 0) echo 'fadeInRight'; else echo 'fadeInLeft'; ?>" data-wow-delay="0.8s">
                                        <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>">
                                    </div>
                                    <div class="service-item__text">
                                        <h2 class="service-item__title wow fadeInUp" data-wow-delay="0.3s"><?php the_title(); ?></h2>
                                        <div class="service-item__text-in wow fadeIn" data-wow-delay="0.5s">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>

                    <!-- Заказать проект -->
                    <div class="wrap">
                        <div class="service-btn ta-c">
                            <?php dynamic_sidebar('order_project'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>