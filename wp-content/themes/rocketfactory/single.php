<?php get_header(); ?>
<!-- page-helper-->
<div class="page-helper">

    <!-- header-->
    <?php include_once('includes/header.php'); ?>

    <!-- page-content-->
    <div class="page-content">

        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <div class="vacancy">
                    <div class="page-title">
                        <div class="wrap">
                            <h1 class="wow fadeInUp"><?php the_title(); ?></h1>
                        </div>
                    </div>
                    <div class="vacancy-about">
                        <div class="wrap clearfix">
                            <div class="vacancy-about__text wow fadeIn" data-wow-delay="0.3s">
                                <!-- Контент  -->
                                <?php the_content(); ?>

                                <div class="meta">
                                    <ul>
                                        <li>Категория <?php the_category(', ') ?></li>
                                        <li><?php comments_popup_link('Нет коментариев', '1 коментарий', '% коментариев'); ?></li>
                                        <?php the_tags('<li>Теги: ', ', ', '</li>'); ?>
                                    </ul>
                                </div>

                                <?php comments_template(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>

            <div class="navigation">
                <div class="next"><?php previous_post_link('%link &raquo;') ?></div>
                <div class="prev"><?php next_post_link('&laquo; %link') ?></div>
            </div>

        <?php else : ?>

            <div class="post">
                <div class="title">
                    <h1>Не найдено</h1>
                </div>
                <div class="content">
                    <p>Извините, ничего не найдено</p>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>
