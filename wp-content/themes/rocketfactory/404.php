<?php get_header(); ?>

<!-- page-helper-->
<div class="page-helper">

    <!-- header-->
    <?php include_once('includes/header.php'); ?>

    <!-- page-content-->
    <div class="page-content">
        <div class="contact">
            <div class="wrap">
                <h1 class="page-title wow fadeInUp">404.</h1>

                <div class="grid-row clearfix">
                    <p>404 Ошибка</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
